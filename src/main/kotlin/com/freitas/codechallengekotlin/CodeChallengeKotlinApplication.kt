package com.freitas.codechallengekotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CodeChallengeKotlinApplication

fun main(args: Array<String>) {
	runApplication<CodeChallengeKotlinApplication>(*args)
}
