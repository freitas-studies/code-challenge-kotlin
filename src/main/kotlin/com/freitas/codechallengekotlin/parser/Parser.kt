package com.freitas.codechallengekotlin.parser

import com.freitas.codechallengekotlin.dto.AnaliseResponse
import com.freitas.codechallengekotlin.dto.ClienteRequest
import com.freitas.codechallengekotlin.dto.EmprestimoResponse
import com.freitas.codechallengekotlin.model.Analise
import com.freitas.codechallengekotlin.model.Cliente
import org.springframework.stereotype.Component

@Component
class Parser {

    fun toModel(cliente: ClienteRequest) =
            Cliente(nome = cliente.nome,
                    cpf = cliente.cpf,
                    idade = cliente.idade,
                    localizacao = cliente.localizacao,
                    salario = cliente.salario)

    fun toDTO(analise: Analise) =
            AnaliseResponse(cliente = analise.cliente,
                            emprestimos = analise.emprestimos
                                                 .map { EmprestimoResponse(it.tipo, it.taxa) }
                                                 .toSet())

    fun teste() = "sera"

}