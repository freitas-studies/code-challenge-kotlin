package com.freitas.codechallengekotlin.model

import java.math.BigDecimal

class Cliente(
        val nome: String,
        val cpf: String,
        val idade: Int,
        val localizacao: Localizacao,
        val salario: BigDecimal
)
