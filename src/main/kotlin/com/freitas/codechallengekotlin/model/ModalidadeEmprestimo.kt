package com.freitas.codechallengekotlin.model

import com.freitas.codechallengekotlin.service.AgregadorRegras
import com.freitas.codechallengekotlin.service.menos30anos
import com.freitas.codechallengekotlin.service.moraEmSP
import com.freitas.codechallengekotlin.service.salarioEntre3000e5000
import com.freitas.codechallengekotlin.service.salarioMaior5000
import com.freitas.codechallengekotlin.service.salarioMenorIgual3000

enum class ModalidadeEmprestimo(
        val tipo: String,
        val taxa: Double,
        private val regras: Set<AgregadorRegras>
) {

    PESSOAL(tipo = "pessoal",
            taxa = 4.0,
            regras = setOf(AgregadorRegras(salarioMenorIgual3000),
                           AgregadorRegras(salarioEntre3000e5000),
                           AgregadorRegras(salarioMaior5000))),

    COM_GARANTIA(tipo = "com garantia",
                 taxa = 3.0,
                 regras = setOf(AgregadorRegras(salarioMenorIgual3000).add(menos30anos).add(moraEmSP),
                                AgregadorRegras(salarioEntre3000e5000).add(moraEmSP),
                                AgregadorRegras(salarioMaior5000).add(menos30anos))),

    CONSIGNADO(tipo = "consignado",
               taxa = 2.0,
               regras = setOf(AgregadorRegras(salarioMaior5000).add(menos30anos)));

    fun analisar(cliente: Cliente) = regras.any { it.processar(cliente) }

}