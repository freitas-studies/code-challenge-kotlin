package com.freitas.codechallengekotlin.model

class Analise(
        val cliente: String,
        val emprestimos: Set<ModalidadeEmprestimo>
)