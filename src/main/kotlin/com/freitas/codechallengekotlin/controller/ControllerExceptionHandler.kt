package com.freitas.codechallengekotlin.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.validation.BindException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.time.LocalDateTime

@RestControllerAdvice
class ControllerExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException::class)
    protected fun processValidationError(ex: MethodArgumentNotValidException): ResponseEntity<ErrorDTO?>? {
        val bindingResult = ex.bindingResult
        return ResponseEntity<ErrorDTO?>(
                ErrorDTO(HttpStatus.BAD_REQUEST, bindingResult.fieldError!!.defaultMessage),
                HttpStatus.BAD_REQUEST)
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = [HttpMessageNotReadableException::class, BindException::class])
    protected fun handleInvalidBody(ex: HttpMessageNotReadableException?): ResponseEntity<ErrorDTO?>? {
        return ResponseEntity<ErrorDTO?>(ErrorDTO(HttpStatus.BAD_REQUEST, "Invalid format!"), HttpStatus.BAD_REQUEST)
    }

}

class ErrorDTO(val status: HttpStatus, val message: String?, val dateTime: LocalDateTime = LocalDateTime.now())
