package com.freitas.codechallengekotlin.controller

import com.freitas.codechallengekotlin.dto.ClienteRequest
import com.freitas.codechallengekotlin.parser.Parser
import com.freitas.codechallengekotlin.service.EmprestimoService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping("api/v1/emprestimos")
class EmprestimoController(private val service: EmprestimoService,
                           private val parser: Parser) {

    @PostMapping
    fun criar(@RequestBody @Valid clienteRequest: ClienteRequest) =
            parser.toModel(clienteRequest)
                    .let(service::analisar)
                    .let(parser::toDTO)
                    .let { ResponseEntity.ok(it) }

}