package com.freitas.codechallengekotlin.dto

data class AnaliseResponse(
        val cliente: String,
        val emprestimos: Set<EmprestimoResponse>
)