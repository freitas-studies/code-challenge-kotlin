package com.freitas.codechallengekotlin.dto

data class EmprestimoResponse(
        val tipo: String,
        val taxa: Double
)
