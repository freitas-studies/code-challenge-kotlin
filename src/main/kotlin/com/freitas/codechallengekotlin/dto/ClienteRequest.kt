package com.freitas.codechallengekotlin.dto

import com.freitas.codechallengekotlin.model.Localizacao
import org.hibernate.validator.constraints.br.CPF
import java.math.BigDecimal
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Positive

data class ClienteRequest(
        
        @field:NotBlank(message = "nome é obrigatório")
        val nome: String,

        @field:NotBlank(message = "cpf é obrigatório")
        @field:CPF(message = "cpf invalido")
        val cpf: String,

        @field:Positive(message = "idade deve ser um valor positivo")
        val idade: Int,

        @field:NotNull
        val localizacao: Localizacao,

        @field:Positive(message = "salario deve ser um valor positivo")
        val salario: BigDecimal
)
