package com.freitas.codechallengekotlin.service

import com.freitas.codechallengekotlin.model.Cliente
import com.freitas.codechallengekotlin.model.Localizacao
import java.math.BigDecimal

fun interface EmprestimoRule {
    fun verificar(cliente: Cliente): Boolean
}

val salarioMenorIgual3000 = EmprestimoRule { it.salario <= BigDecimal(3000) }
val salarioEntre3000e5000 = EmprestimoRule { it.salario > BigDecimal(3000) && it.salario < BigDecimal(5000) }
val salarioMaior5000 = EmprestimoRule { it.salario >= BigDecimal(5000) }

val moraEmSP = EmprestimoRule { it.localizacao == Localizacao.SP }
val menos30anos = EmprestimoRule { it.idade < 30 }

class AgregadorRegras(rule: EmprestimoRule) {
    private val rules = arrayListOf<EmprestimoRule>()

    init {
        rules.add(rule)
    }

    fun add(rule: EmprestimoRule): AgregadorRegras {
        rules.add(rule)
        return this
    }

    fun processar(cliente: Cliente) = rules.all { it.verificar(cliente) }
}

