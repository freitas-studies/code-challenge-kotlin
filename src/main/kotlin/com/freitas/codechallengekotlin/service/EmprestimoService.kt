package com.freitas.codechallengekotlin.service

import com.freitas.codechallengekotlin.model.Analise
import com.freitas.codechallengekotlin.model.Cliente
import com.freitas.codechallengekotlin.model.ModalidadeEmprestimo
import org.springframework.stereotype.Service

@Service
class EmprestimoService {

    fun analisar(cliente: Cliente) = Analise(cliente = cliente.nome,
                                             emprestimos = ModalidadeEmprestimo.values()
                                                                               .asList()
                                                                               .filter { it.analisar(cliente) }
                                                                               .toSet())

}