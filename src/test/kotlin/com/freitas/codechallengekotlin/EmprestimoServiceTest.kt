package com.freitas.codechallengekotlin

import com.freitas.codechallengekotlin.model.Cliente
import com.freitas.codechallengekotlin.model.Localizacao
import com.freitas.codechallengekotlin.model.ModalidadeEmprestimo
import com.freitas.codechallengekotlin.service.EmprestimoService
import io.kotest.core.spec.style.FeatureSpec
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldNotContain
import io.kotest.matchers.shouldBe
import java.math.BigDecimal

class EmprestimoServiceTest : FeatureSpec({

    val emprestimoService = EmprestimoService()

    feature("Emprestimo pessoal") {
        listOf(
                Cliente(nome = "nome", cpf = "1", idade = 30, localizacao = Localizacao.SP, salario = BigDecimal(1000)),
                Cliente(nome = "nome", cpf = "1", idade = 30, localizacao = Localizacao.SP, salario = BigDecimal(3000)),
                Cliente(nome = "nome", cpf = "1", idade = 30, localizacao = Localizacao.SP, salario = BigDecimal(3001)),
                Cliente(nome = "nome", cpf = "1", idade = 30, localizacao = Localizacao.SP, salario = BigDecimal(5000)),
                Cliente(nome = "nome", cpf = "1", idade = 30, localizacao = Localizacao.SP, salario = BigDecimal(5001))
        ).forEach {
            val analise = emprestimoService.analisar(it)
            analise.cliente shouldBe it.nome
            analise.emprestimos shouldContain ModalidadeEmprestimo.PESSOAL
        }
    }

    feature("Emprestimo com garantia") {

        scenario("Deve conter o emprestimo com garantia") {
            listOf(
                    Cliente(nome = "nome", cpf = "1", idade = 20, localizacao = Localizacao.SP, salario = BigDecimal(1000))
                            to setOf(ModalidadeEmprestimo.PESSOAL, ModalidadeEmprestimo.COM_GARANTIA),
                    Cliente(nome = "nome", cpf = "1", idade = 29, localizacao = Localizacao.SP, salario = BigDecimal(3000))
                            to setOf(ModalidadeEmprestimo.PESSOAL, ModalidadeEmprestimo.COM_GARANTIA),
                    Cliente(nome = "nome", cpf = "1", idade = 29, localizacao = Localizacao.SP, salario = BigDecimal(5001))
                            to setOf(ModalidadeEmprestimo.PESSOAL, ModalidadeEmprestimo.COM_GARANTIA, ModalidadeEmprestimo.CONSIGNADO)
            ).forEach { (cliente, modalidadeEsperada) ->
                val analise = emprestimoService.analisar(cliente)
                analise.cliente shouldBe cliente.nome
                analise.emprestimos shouldBe modalidadeEsperada
            }
        }

        scenario("Não deve conter o emprestimo com garantia") {
            listOf(
                    Cliente(nome = "nome", cpf = "1", idade = 30, localizacao = Localizacao.SP, salario = BigDecimal(1000))
                            to ModalidadeEmprestimo.COM_GARANTIA,
                    Cliente(nome = "nome", cpf = "1", idade = 29, localizacao = Localizacao.RJ, salario = BigDecimal(1000))
                            to ModalidadeEmprestimo.COM_GARANTIA
            ).forEach { (cliente, modalidadeEsperada) ->
                val analise = emprestimoService.analisar(cliente)
                analise.cliente shouldBe cliente.nome
                analise.emprestimos shouldNotContain modalidadeEsperada
            }
        }
    }

    feature("Emprestimo consignado") {

        scenario("Deve conter emprestimo consignado") {
            listOf(
                    Cliente(nome = "nome", cpf = "1", idade = 20, localizacao = Localizacao.RJ, salario = BigDecimal(5001)),
                    Cliente(nome = "nome", cpf = "1", idade = 23, localizacao = Localizacao.SP, salario = BigDecimal(5000))
            ).forEach {
                val analise = emprestimoService.analisar(it)
                analise.cliente shouldBe it.nome
                analise.emprestimos shouldContain ModalidadeEmprestimo.CONSIGNADO
            }
        }

        scenario("Não deve conter emprestimo consignado") {
            listOf(
                    Cliente(nome = "nome", cpf = "1", idade = 30, localizacao = Localizacao.RJ, salario = BigDecimal(5001)),
                    Cliente(nome = "nome", cpf = "1", idade = 23, localizacao = Localizacao.RS, salario = BigDecimal(4999))
            ).forEach {
                val analise = emprestimoService.analisar(it)
                analise.cliente shouldBe it.nome
                analise.emprestimos shouldNotContain ModalidadeEmprestimo.CONSIGNADO
            }
        }
    }

})