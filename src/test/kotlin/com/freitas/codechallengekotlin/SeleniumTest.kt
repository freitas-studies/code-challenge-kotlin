package com.freitas.codechallengekotlin

import com.freitas.codechallengekotlin.container.WebDriverContainer
import com.freitas.codechallengekotlin.model.Cliente
import com.freitas.codechallengekotlin.model.Localizacao
import io.kotest.core.spec.style.FeatureSpec
import org.openqa.selenium.By
import java.math.BigDecimal


class SeleniumTest : FeatureSpec({

    var browser = WebDriverContainer().webDriver

    feature("Emprestimo pessoal") {
        listOf(
            Cliente(nome = "nome", cpf = "1", idade = 30, localizacao = Localizacao.SP, salario = BigDecimal(5001))
        ).forEach { it ->

            println("Input: $it")

            browser.get("https://waitbutwhy.com/archive")

            browser.findElementsByClassName("post-right")
                .map { it.findElement(By.tagName("H5")) }
                .map { it.findElement(By.tagName("A")) }
                .map { "output: ${it.text}" }
                .forEach(::println)
        }
    }


})