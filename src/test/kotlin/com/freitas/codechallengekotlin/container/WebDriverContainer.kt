package com.freitas.codechallengekotlin.container

import org.openqa.selenium.chrome.ChromeOptions
import org.testcontainers.containers.BrowserWebDriverContainer

class WebDriverContainer : BrowserWebDriverContainer<WebDriverContainer>() {

    init {
        withCapabilities(ChromeOptions().setHeadless(true))
        withRecordingMode(BrowserWebDriverContainer.VncRecordingMode.SKIP, null)
        start()
    }

}
